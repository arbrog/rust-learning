use criterion::{black_box, criterion_group, criterion_main, Criterion};
use rust_challenger::calc_average::find_average;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("calc_ave", |b| {
        b.iter(|| {
            find_average(black_box(&[
                17.0, 16.0, 16.0, 16.0, 16.0, 15.0, 17.0, 17.0, 15.0, 5.0, 17.0, 17.0, 16.0,
                17.0, 16.0, 16.0, 16.0, 16.0, 15.0, 17.0, 17.0, 15.0, 5.0, 17.0, 17.0, 16.0,
            ]))
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
