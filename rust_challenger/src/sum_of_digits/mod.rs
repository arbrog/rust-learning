pub fn digital_root(n: i64) -> i64 {
    if n / 10 == 0 {
        return n;
    }
    let mut temp = n;
    let mut next = 0;
    while temp != 0 {
        next += n % 10;
        temp = temp / 10;
    }
    return digital_root(next);
}
