pub fn find_average(slice: &[f64]) -> f64 {
    if slice.len() == 0 {
        return 0.0;
    }
    let inter = slice.iter().fold(0 as f64, |accum, item| accum + item);
    return inter / slice.len() as f64;
}
