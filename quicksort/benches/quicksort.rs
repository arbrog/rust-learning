use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use quicksort::rayonQuickSort::rayonQuickSort::quick_sort;
use quicksort::{
    quickSortMulti, quickSortSingle::quickSortSingle, rayonQuickSort::rayonQuickSort::Parallel,
};
use rand::{rngs::StdRng, Rng, SeedableRng};

fn setup_test(sample: u32, rng: &mut StdRng, storage: &mut Vec<Vec<Vec<i32>>>) {
    //setup x test
    let mut storage_x = Vec::new();
    let mut to_sortx = Vec::new();
    for _ in 0..sample {
        to_sortx.push(rng.gen_range(0..1000));
    }
    storage_x.push(to_sortx.clone());
    storage_x.push(to_sortx.clone());
    storage_x.push(to_sortx);
    storage.push(storage_x);
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(0xA4028DF);
    let mut storage = Vec::new();
    const BASE: u32 = 10;
    setup_test(BASE.pow(2), &mut rng, &mut storage);
    setup_test(BASE.pow(3), &mut rng, &mut storage);
    setup_test(BASE.pow(4), &mut rng, &mut storage);
    setup_test(BASE.pow(4) * 5, &mut rng, &mut storage);
    setup_test(BASE.pow(5), &mut rng, &mut storage);
    // setup_test(BASE.pow(6), &mut rng, &mut storage);

    //run bench
    let mut group = c.benchmark_group("quicksort");
    for (i, test) in [
        "random100",
        "random1000",
        "random10000",
        "random50000",
        "random100000",
        // "random1000000",
    ]
    .iter()
    .enumerate()
    {
        let length = storage[i][0].len();
        group.throughput(Throughput::Elements(length as u64));
        if length > 100000 {
            group.sample_size(10);
        }
        group.bench_with_input(BenchmarkId::new("SingleThread", test), &i, |b, i| {
            b.iter(|| quickSortSingle::quick_sort(&mut storage[*i][0], 0, length - 1))
        });
        group.bench_with_input(BenchmarkId::new("MultiThread", test), &i, |b, i| {
            b.iter(|| quickSortMulti::quick_sort_multi::quick_sort(&mut storage[*i][1]))
        });
        group.bench_with_input(BenchmarkId::new("MultiThreadRayon", test), &i, |b, i| {
            b.iter(|| quick_sort::<Parallel, i32>(&mut storage[*i][2]))
        });
    }
    group.finish();
}

fn micro_bench(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(0xA4028DF);
    let mut storage = Vec::new();
    const BASE: u32 = 10;
    setup_test(BASE.pow(5) * 1, &mut rng, &mut storage);

    //run bench
    let mut group = c.benchmark_group("micro_quicksort");
    let length = storage[0][0].len();
    group.sample_size(10);
    group.throughput(Throughput::Elements(length as u64));
    group.bench_with_input(
        BenchmarkId::new("SingleThread", "1000000"),
        "dasd",
        |b, _| b.iter(|| quickSortSingle::quick_sort(&mut storage[0][0], 0, length - 1)),
    );
    group.bench_with_input(
        BenchmarkId::new("MultiThread", "1000000"),
        "DAsd",
        |b, _| b.iter(|| quickSortMulti::quick_sort_multi::quick_sort(&mut storage[0][1])),
    );
    group.bench_with_input(
        BenchmarkId::new("RayonMultiThread", "1000000"),
        "DASd",
        |b, _| b.iter(|| quick_sort::<Parallel, i32>(&mut storage[0][2])),
    );

    group.finish();
}

// criterion_group!(benches, micro_bench, criterion_benchmark);
criterion_group!(benches, micro_bench);
criterion_main!(benches);
