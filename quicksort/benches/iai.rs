use iai::{black_box, main};
use quicksort::{quickSortMulti::quick_sort_multi, quickSortSingle::quickSortSingle};
use rand::{rngs::StdRng, Rng, SeedableRng};

fn micro_bench_single() {
    let mut rng = StdRng::seed_from_u64(0xA4028DF);

    let num_elements = 1000;
    let mut to_sort = Vec::new();
    for _ in 0..num_elements {
        to_sort.push(rng.gen_range(1..1000));
    }
    let length = to_sort.len();
    quickSortSingle::quick_sort(black_box(&mut to_sort), black_box(0), black_box(length - 1))
}

fn micro_bench_multi() {
    let mut rng = StdRng::seed_from_u64(0xA4028DF);

    let num_elements = 1000;
    let mut to_sort = Vec::new();
    for _ in 0..num_elements {
        to_sort.push(rng.gen_range(1..1000));
    }
    quick_sort_multi::quick_sort(black_box(&mut to_sort))
}

iai::main!(micro_bench_single, micro_bench_multi);
