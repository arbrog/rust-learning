use std::time::{Duration, Instant};

use quicksort::quickSortMulti::quick_sort_multi;
use quicksort::quickSortSingle::quickSortSingle;

fn main() {
    use rand::prelude::StdRng;
    use rand::Rng;
    use rand::SeedableRng;
    // let mut input = vec![5, 4, 3, 2];
    let mut rng = StdRng::seed_from_u64(0x94228DF);
    let mut input = Vec::new();
    for _ in 0..15 {
        input.push(rng.gen_range(1..1000));
    }
    // let length = input.len();
    // quickSortSingle::quick_sort(&mut input, 0, length - 1);
    // println!("{:?}", input);
    // println!("{:?}", input);
    quick_sort_multi::quick_sort(&mut input);
    println!("{:?}", input);
}

fn main2() {
    use rand::prelude::StdRng;
    use rand::Rng;
    use rand::SeedableRng;

    let mut rng = StdRng::seed_from_u64(0xA4028DF);
    let mut accum = Duration::new(0, 0);
    let mut accum2 = Duration::new(0, 0);
    let full = Instant::now();
    const RUNS: u32 = 5;
    for _ in 0..RUNS {
        let num_elements = 50000;
        let mut to_sort = Vec::new();
        for _ in 0..num_elements {
            to_sort.push(rng.gen_range(1..1000));
        }
        let mut to_sort2 = to_sort.clone();
        let length = to_sort.len();

        let mut start = Instant::now();
        quickSortSingle::quick_sort(&mut to_sort, 0, length - 1);
        accum += start.elapsed();

        start = Instant::now();
        quick_sort_multi::quick_sort(&mut to_sort2);
        accum2 += start.elapsed();
    }
    println!("Time elapsed in total is: {:?}", full.elapsed());
    println!("Time elapsed in quick_sort_single() is: {:?}", accum / RUNS);
    println!("Time elapsed in quick_sort_multi() is: {:?}", accum2 / RUNS);
    println!(
        "Percent Difference is is: {:?}%",
        ((accum.as_nanos() as i64 - accum2.as_nanos() as i64) as f64 / accum2.as_nanos() as f64)
            * 100 as f64
    );
}
