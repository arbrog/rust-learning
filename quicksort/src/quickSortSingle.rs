pub mod quickSortSingle {

    pub fn quick_sort(input: &mut [i32], start: usize, end: usize) {
        if start < end {
            let pi = partition(input, start, end);
            // println!("partition=={}", pi);
            // println!("{:?}", input);
            let mut lower_end = pi as isize - 1;
            if lower_end < 0 {
                lower_end = 0;
            }
            quick_sort(input, start, lower_end as usize); // Before pi
            quick_sort(input, (pi + 1) as usize, end); // After pi
                                                       // println!("{:?}", input);
        }
    }

    fn partition(input: &mut [i32], start: usize, end: usize) -> usize {
        let pivot = input[end];
        let mut i = start;
        for j in start..end {
            if input[j] < pivot {
                //swap the two
                swap(input, i, j);
                i += 1;
            }
        }
        swap(input, i, end);
        return i;
    }

    fn swap(input: &mut [i32], i: usize, j: usize) {
        let temp = input[i];
        input[i] = input[j];
        input[j] = temp;
    }
}

mod tests {
    use super::*;

    #[allow(dead_code)]
    fn sort_test(input: &mut Vec<i32>) {
        let length = input.len();
        quickSortSingle::quick_sort(input, 0, length - 1);
        is_sorted(input);
    }

    #[allow(dead_code)]
    fn is_sorted(input: &Vec<i32>) {
        let length = input.len();
        for (index, value) in input.iter().enumerate() {
            if index < length - 1 {
                if value > &input[index + 1] {
                    assert!(false);
                } else {
                    assert!(true);
                }
            }
        }
    }

    #[test]
    fn basic_sort() {
        let mut input = vec![50, 23, 9, 18, 61, 32, 0, -10];
        sort_test(&mut input);
    }
    #[test]
    fn inverse_sort() {
        let mut input = vec![5, 4, 3, 2, 1];
        sort_test(&mut input);
    }
    #[test]
    fn already_sort() {
        let mut input = vec![1, 2, 3, 4, 5];
        sort_test(&mut input);
    }
    #[test]
    fn one_sort() {
        let mut input = vec![2];
        sort_test(&mut input);
    }
    #[test]
    fn two_sort() {
        let mut input = vec![5, 2];
        sort_test(&mut input);
    }

    #[test]
    fn random_test_suite() {
        use rand::prelude::StdRng;
        use rand::Rng;
        use rand::SeedableRng;

        let mut rng = StdRng::seed_from_u64(0xA4028DF);
        for _ in 0..1000 {
            let num_elements = rng.gen_range(1..100);
            let mut to_sort = Vec::new();
            for _ in 0..num_elements {
                to_sort.push(rng.gen_range(1..1000));
            }
            sort_test(&mut to_sort);
        }
    }

    #[test]
    fn worst_case_suite() {
        for _ in 0..1 {
            let num_elements = 1000;
            let mut to_sort = Vec::new();
            for i in 0..num_elements {
                to_sort.push(num_elements - i);
            }
            sort_test(&mut to_sort);
        }
    }
}
