pub mod quick_sort_multi {
    use crossbeam::thread::Scope;

    fn quick_sort_helper<'a>(input: &'a mut [i32], scope: &Scope<'a>) {
        let input_len = input.len();
        if input_len > 0 {
            let pi = partition(input);
            let lower_end: usize = pi;
            let (lower_slice, remainder) = input.split_at_mut(lower_end as usize);
            let (_, upper_slice) = remainder.split_at_mut(1);
            if lower_slice.len() > 10000 {
                let lower_handle =
                    scope.spawn(move |inner_scope| quick_sort_helper(lower_slice, inner_scope));
                lower_handle.join().unwrap();
            } else {
                quick_sort_helper(lower_slice, scope);
            }
            if upper_slice.len() > 10000 {
                let upper_handle =
                    scope.spawn(move |inner_scope| quick_sort_helper(upper_slice, inner_scope));
                upper_handle.join().unwrap();
            } else {
                quick_sort_helper(upper_slice, scope);
            }
        }
    }

    pub fn quick_sort(input: &mut [i32]) {
        crossbeam::scope(|scope| quick_sort_helper(input, scope)).unwrap()
    }

    fn partition(input: &mut [i32]) -> usize {
        let length = input.len();
        let pivot = input[length - 1];
        let mut i = 0;
        for j in 0..length {
            if input[j] < pivot {
                if i != j {
                    input.swap(i, j);
                }
                //swap the two
                i += 1;
            }
        }
        if i != length - 1 {
            input.swap(i, length - 1);
        }
        return i;
    }
}

mod tests {
    use super::*;

    #[allow(dead_code)]
    fn sort_test(input: &mut Vec<i32>) {
        quick_sort_multi::quick_sort(input);
        is_sorted(input);
    }

    #[allow(dead_code)]
    fn is_sorted(input: &Vec<i32>) {
        let length = input.len();
        for (index, value) in input.iter().enumerate() {
            if index < length - 1 {
                if value > &input[index + 1] {
                    assert!(false);
                } else {
                    assert!(true);
                }
            }
        }
    }

    #[test]
    fn basic_sort() {
        let mut input = vec![50, 23, 9, 18, 61, 32, 0, -10];
        sort_test(&mut input);
    }
    #[test]
    fn inverse_sort() {
        let mut input = vec![5, 4, 3, 2, 1];
        sort_test(&mut input);
    }
    #[test]
    fn inverse_sort_short() {
        let mut input = vec![5, 4, 3];
        sort_test(&mut input);
    }
    #[test]
    fn already_sort() {
        let mut input = vec![1, 2, 3, 4, 5];
        sort_test(&mut input);
    }
    #[test]
    fn one_sort() {
        let mut input = vec![2];
        sort_test(&mut input);
    }
    #[test]
    fn two_sort() {
        let mut input = vec![5, 2];
        sort_test(&mut input);
    }

    #[test]
    fn random_test_suite() {
        use rand::prelude::StdRng;
        use rand::Rng;
        use rand::SeedableRng;

        let mut rng = StdRng::seed_from_u64(0xA4028DF);
        for _ in 0..1000 {
            let num_elements = rng.gen_range(1..100);
            let mut to_sort = Vec::new();
            for _ in 0..num_elements {
                to_sort.push(rng.gen_range(1..1000));
            }
            sort_test(&mut to_sort);
        }
    }

    #[test]
    fn worst_case_suite() {
        for _ in 0..1 {
            let num_elements = 1000;
            let mut to_sort = Vec::new();
            for i in 0..num_elements {
                to_sort.push(num_elements - i);
            }
            sort_test(&mut to_sort);
        }
    }
}
